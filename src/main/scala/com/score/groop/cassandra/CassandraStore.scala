package com.score.groop.cassandra

import java.util.concurrent.Executors
import java.util.{Date, UUID}

import com.datastax.driver.core.querybuilder.QueryBuilder
import com.datastax.driver.core.utils.UUIDs
import com.score.groop.config.SchemaConf
import com.score.groop.util.AppLogger

import scala.collection.JavaConverters._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}

case class Trans(id: String, execer: String, actor: String, message: String,
                 watchId: String, watchModel: String, watchColor: String, watchOwner: String,
                 timestamp: Date = new Date)

case class Block(id: UUID = UUIDs.timeBased(), grooper: String, trans: List[Trans], merkelRoot: String, preHash: String,
                 hash: String, timestamp: Date = new Date())

object CassandraStore extends CassandraCluster with SchemaConf with AppLogger {

  lazy val gtps = session.prepare("SELECT * FROM mystiko.trans WHERE execer = ? AND id = ?")
  lazy val cbps = session.prepare("INSERT INTO mystiko.blocks(grooper, id, trans, merkle_root, pre_hash, hash, timestamp) VALUES(?, ?, ?, ?, ?, ?, ?)")

  def init() = {
    // create keyspace
    // create udt type
    // create table
    session.execute(schemaCreateKeyspace)
    session.execute(schemaCreateTypeTrans)
    session.execute(schemaCreateTableTrans)
    session.execute(schemaCreateTableBlocks)
  }

  def getTrans(ids: List[String]): (List[String], List[Option[Trans]]) = {
    def get(id: String): Future[Option[Trans]] = {
      Future {
        val attr = id.split(";")
        val row = session.execute(gtps.bind(attr(0), attr(1))).one()
        if (row != null) {
          Option(Trans(
            row.getString("execer"),
            row.getString("id"),
            row.getString("actor"),
            row.getString("message"),
            row.getString("watch_id"),
            row.getString("watch_model"),
            row.getString("watch_color"),
            row.getString("watch_owner"),
            row.getTimestamp("timestamp"))
          )
        } else {
          None
        }
      }
    }

    // get trans from 10 futures
    implicit val ec = ExecutionContext.fromExecutorService(Executors.newWorkStealingPool(10))
    val f = Future.sequence(ids.map(p => get(p)))
    Await.result(f, 30.seconds) match {
      case l: List[Option[Trans]] =>
        logger.info(s"Success get trans, $l")
        (ids, l)
      case _ =>
        logger.error(s"Fail get trans, timeout")
        (ids, List())
    }
  }

  def createBlock(block: Block) = {
    Future {
      // trans UDT
      val transType = cluster.getMetadata.getKeyspace(cassandraKeyspace).getUserType("trans")
      val trans = block.trans.map(t =>
        transType.newValue
          .setString("execer", t.execer)
          .setString("id", t.id)
          .setString("actor", t.actor)
          .setString("message", t.message)
          .setString("watch_id", t.watchId)
          .setString("watch_model", t.watchModel)
          .setString("watch_color", t.watchColor)
          .setString("watch_owner", t.watchOwner)
          .setTimestamp("timestamp", t.timestamp)
      ).asJava

      // insert query
      val statement = QueryBuilder.insertInto(cassandraKeyspace, "blocks")
        .value("grooper", block.grooper)
        .value("id", block.id)
        .value("trans", trans)
        .value("merkle_root", block.merkelRoot)
        .value("pre_hash", block.preHash)
        .value("hash", block.hash)
        .value("timestamp", block.timestamp)

      session.execute(statement)
      block
    }
  }
}

//object M extends App {
//  CassandraStore.init()
//  val t = CassandraStore.getTrans(List("eranga;112"))
//  val b = BlockFactory.block(t._2.flatten)
//  CassandraStore.createBlock(b)
//  println(t)
//}
